﻿using ContosoUniversity.FunctionalTests.Config;
using ContosoUniversity.FunctionalTests.Navigation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace ContosoUniversity.FunctionalTests
{
    [TestClass]
    public abstract class TestBase
    {
        public static IisExpressWebServer WebServer;
        public static IWebDriver Browser;
        private static MvcUrlHelper _mvcUrlHelper ;

        [TestInitialize]
        public void Initialize()
        {
            var app = new WebApplication(ProjectLocation.FromFolder("ContosoUniversity"), 12365);
            app.AddEnvironmentVariable("FunctionalTests");
            WebServer = new IisExpressWebServer(app);
            WebServer.Start("Release");
            _mvcUrlHelper = new MvcUrlHelper(RouteConfig.RegisterRoutes(new RouteCollection()));
        }

        [TestCleanup]
        public void Cleanup()
        {
            Browser.Quit();
            WebServer.Stop();
        }

        public static TPage NavigateTo<TController, TPage>(Expression<Action<TController>> action)
            where TController : Controller
            where TPage : new()
        {
            string relativeUrl = _mvcUrlHelper.GetRelativeUrlFor(action);
            string url = string.Format("{0}{1}", WebServer.BaseUrl, relativeUrl);
            Browser.Navigate().GoToUrl(url);
            return new TPage();
        }
    }
}