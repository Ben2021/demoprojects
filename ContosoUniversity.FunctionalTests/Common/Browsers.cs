﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace ContosoUniversity.FunctionalTests
{
    public static class Browsers
    {
        private static RemoteWebDriver _chrome;
        public static RemoteWebDriver Chrome
        {
            get
            {
                if (_chrome == null) _chrome = new ChromeDriver();
                return _chrome;
            }
        }

        private static RemoteWebDriver _ie;
        public static RemoteWebDriver IE
        {
            get
            {
                if (_ie == null) _ie = new InternetExplorerDriver();
                return _ie;
            }
        }
    }
}
