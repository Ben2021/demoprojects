﻿using ContosoUniversity.Controllers;
using ContosoUniversity.FunctionalTests.Navigation;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Routing;

namespace ContosoUniversity.FunctionalTests
{
    [TestClass]
    public class RouteUnitTests : TestBase
    {
        #region Class Initializer 

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Browser = Browsers.IE;
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
            //TODO
        }

        #endregion

        [TestMethod]
        public void MvcUrlHelper_should_return_correct_route_for_controller_action()
        {
            var routes = RouteConfig.RegisterRoutes(new RouteCollection());
            var sut = new MvcUrlHelper(routes);

            sut.GetRelativeUrlFor<StudentController>(x => x.Details(1))
                .Should().Be("/Student/Details/1");
        }
    }
}
