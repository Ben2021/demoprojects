﻿using ContosoUniversity.Controllers;
using ContosoUniversity.FunctionalTests.PageObjects;
using ContosoUniversity.ResourceFiles;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContosoUniversity.FunctionalTests.Tests
{
    [TestClass]
    public class MagicStringTests : TestBase
    {
        #region Class Initializer 

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Browser = Browsers.IE;
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
            //TODO
        }

        #endregion

        [TestMethod]
        public void CanNavigateToPageViaControllerAction()
        {
            var newStudentPage = TestBase.NavigateTo<StudentController, NewStudentPage>(x => x.Create());
            newStudentPage.Url.Should().Be(TestBase.WebServer.BaseUrl + "/Student/Create");
        }

        [TestMethod]
        public void CanAccessResourceFilesFromTests()
        {
            var newStudentPage = TestBase.NavigateTo<StudentController, NewStudentPage>(x => x.Create());
            newStudentPage.HeaderTitle.Should().Be(Resources.Student_CreateForm_Title);
        }

        [TestMethod]
        public void CanIdentifyPageWithHiddenIdentifier()
        {
            var newStudentPage = TestBase.NavigateTo<StudentController, NewStudentPage>(x => x.Create());
            newStudentPage.PageId.Should().Be(LocalSiteMap.Pages.Student.Create);
        }
    }
}
