﻿using ContosoUniversity.Controllers;
using ContosoUniversity.FunctionalTests.PageObjects;
using ContosoUniversity.ViewModels;
using FizzWare.NBuilder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace ContosoUniversity.FunctionalTests.Tests
{
    [TestClass]
    public class StudentPageTests : TestBase
    {
        #region Class Initializer 

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Browser = Browsers.IE;
        }
        
        [ClassCleanup()]
        public static void ClassCleanup()
        {
            //TODO
        }

        #endregion

        [TestMethod]
        public void CanPopulateAFormFieldFromModelProperty()
        {
            var student = Builder<CreateStudentForm>.CreateNew().Build();
            var newStudentPage = TestBase.NavigateTo<StudentController, NewStudentPage>(x => x.Create());

            newStudentPage
                .InputLastName(student.LastName)
                .InputFirstName(student.FirstMidName)       
                .InputEnrollmentDate(student.EnrollmentDate);          

            TestBase.Browser.FindElement(By.Id("FirstMidName")).GetAttribute("value").Should().Be(student.FirstMidName);
            TestBase.Browser.FindElement(By.Id("LastName")).GetAttribute("value").Should().Be(student.LastName);
            TestBase.Browser.FindElement(By.Id("EnrollmentDate")).GetAttribute("value").Should().Be(student.EnrollmentDate.ToString());

            var detailsPage = newStudentPage.Save();
            detailsPage.SearchLastName(student.LastName);

            TestBase.Browser.FindElement(By.Id("item_LastName")).Text.Should().Be(student.LastName);
            TestBase.Browser.FindElement(By.Id("item_FirstMidName")).Text.Should().Be(student.FirstMidName);            
        }

        //[TestMethod]
        //public void CanPopulateFormFromModel()
        //{
        //    var student = Builder<CreateStudentForm>
        //        .CreateNew()
        //        .Build();
        //    var newStudentPage = Host.NavigateTo<StudentController, NewStudentPage>(x => x.Create());

        //    newStudentPage.AddValidStudent(student);

        //    Host.Browser.FindElement(By.Id("FirstMidName")).GetAttribute("value").Should().Be(student.FirstMidName);
        //    Host.Browser.FindElement(By.Id("LastName")).GetAttribute("value").Should().Be(student.LastName);
        //    Host.Browser.FindElement(By.Id("EnrollmentDate")).GetAttribute("value").Should().Be(student.EnrollmentDate.ToString());
        //}

        //[TestMethod]
        //public void CanReadFormFieldFromModelProperty()
        //{
        //    var studentDetailsPage = Host.NavigateTo<StudentController, StudentDetailsPage>(x => x.Details(1));

        //    studentDetailsPage
        //        .DisplayFor(x => x.LastName)
        //        .Should().Be("Alexander");
        //}

        //[TestMethod]
        //public void CanReadModelFromPage()
        //{
        //    var studentDetailsPage = Host.NavigateTo<StudentController, StudentDetailsPage>(x => x.Details(1));

        //    var model = studentDetailsPage.ReadModel();

        //    model.FirstMidName.Should().Be("Carson");
        //    model.LastName.Should().Be("Alexander");
        //}

    }
}
