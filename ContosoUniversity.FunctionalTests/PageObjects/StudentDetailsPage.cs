﻿using ContosoUniversity.Models;
using OpenQA.Selenium;

namespace ContosoUniversity.FunctionalTests.PageObjects
{
    public class StudentDetailsPage : Page<StudentDetailsViewModel>
    {
        public void SearchLastName(string lastName)
        {
            TextBoxFor(x => x.SearchString, lastName);
            TestBase.Browser.FindElement(By.Name("searchBtn")).Click();
        }
    }

    public class StudentDetailsViewModel
    {
        public string LastName { get; set; }
        public string FirstMidName { get; set; }

        public string SearchString { get; set; }
    }
}